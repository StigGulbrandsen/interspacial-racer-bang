//making a window

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <camera.h>
#include <model.h>
#include <chunk.h>
#include <irrKlang.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

using namespace irrklang;
//#pragma comment(lib, "/lib/irrKlang-1.6.0/bin/linux-gcc-64/irrKlang.so") // link with irrKlang.dll

ISoundEngine *SoundEngine = createIrrKlangDevice();


void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
unsigned int loadTexture(const char *path);
unsigned int loadCubemap(std::vector<std::string> faces);

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

//camera vectors
Camera camera(glm::vec3(-20.0f, 0.0f, -1.0f));


//time between current frame and last frame
float deltaTime = 0.0f;
//time of last frame
float lastFrame = 0.0f;

float lastX = SCR_WIDTH/2;
float lastY = SCR_HEIGHT/2;

bool firstMouse = true;
bool init = false;
bool mousePointer;
std::clock_t start;

glm::vec3 lightPos(0.6f, 0.0f, -1.0f);

glm::mat4 model;

//used to the procedural generation
int chunkCount = 0;
std::vector<Chunk> chunkArray;

//vertices of the cube
 float vertices[] = {
  // positions          // normals           // texture coords
        -0.5f, -0.5f, -0.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.0f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.0f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
         0.5f,  0.5f, -0.0f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
        -0.5f,  0.5f, -0.0f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
        -0.5f, -0.5f, -0.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
         0.5f,  0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
        -0.5f,  0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

        -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.0f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
        -0.5f, -0.5f, -0.0f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f, -0.0f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

         0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
         0.5f, -0.5f, -0.0f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f, -0.0f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

        -0.5f, -0.5f, -0.0f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f, -0.0f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
         0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
         0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
        -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.0f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f, -0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f, -0.0f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
         0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
};


//vertices of the outer walls
float wallVertices[] = {
    // positions // normals // texture coords
    -3000.0f,-3000.0f, -0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
     3000.0f,-3000.0f, -0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
     3000.0f, 3000.0f, -0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
     3000.0f, 3000.0f, -0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
    -3000.0f, 3000.0f, -0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
    -3000.0f,-3000.0f, -0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,

    -3000.0f,-3000.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
     3000.0f,-3000.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
     3000.0f, 3000.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
     3000.0f, 3000.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
    -3000.0f, 3000.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
    -3000.0f,-3000.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    -3000.0f, 3000.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    -3000.0f, 3000.0f, -0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
    -3000.0f,-3000.0f, -0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    -3000.0f,-3000.0f, -0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    -3000.0f,-3000.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    -3000.0f, 3000.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    3000.0f, 3000.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    3000.0f, 3000.0f, -0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
    3000.0f,-3000.0f, -0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    3000.0f,-3000.0f, -0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    3000.0f,-3000.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    3000.0f, 3000.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

   -3000.0f,-3000.0f, -0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
    3000.0f, -3000.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
    3000.0f, -3000.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
    3000.0f, -3000.0f, -0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
   -3000.0f,-3000.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
   -3000.0f,-3000.0f, -0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,

    -3000.0f, 3000.0f, -0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
     3000.0f, 3000.0f, -0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
     3000.0f, 3000.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
     3000.0f, 3000.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
    -3000.0f, 3000.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
    -3000.0f, 3000.0f, -0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
};

//Position of the speed rings
glm::vec3 goldRingPositions[] = {
	glm::vec3(50.7f,  0.2f,  2.0f),
	glm::vec3(900.3f, -3.3f, -10.0f),
	glm::vec3(1750.0f,  2.0f, -12.0f),
	glm::vec3(2500.0f,  0.0f, 20.0f),
    glm::vec3(3350.3f, -3.3f, -10.0f),
	glm::vec3(4100.0f,  2.0f, 12.0f),
	glm::vec3(4900.0f,  0.0f, -30.0f),
    glm::vec3(8750.3f, -3.3f, -10.0f),
	glm::vec3(9550.0f,  2.0f, 12.0f),
	glm::vec3(10350.0f,  0.0f, -40.0f),
    glm::vec3(11150.3f, -3.3f, 10.0f),
	glm::vec3(11950.0f,  2.0f, -12.0f),
	glm::vec3(12750.0f,  0.0f, 40.0f)
};  

//Position of the nanosuits and varia suits floating in space
glm::vec3 nanosuitPositions[] = {
	glm::vec3(150.7f,  0.2f,  60.0f),
	glm::vec3(1000.3f, -3.3f, -60.0f),
	glm::vec3(1850.0f,  2.0f, -72.0f),
	glm::vec3(2600.0f,  0.0f, 80.0f),
    glm::vec3(3450.3f, -3.3f, -70.0f),
	glm::vec3(4200.0f,  2.0f, 72.0f),
	glm::vec3(5000.0f,  0.0f, -90.0f),
    glm::vec3(8850.3f, -3.3f, -70.0f),
	glm::vec3(9650.0f,  2.0f, 72.0f),
	glm::vec3(10450.0f,  0.0f, -100.0f),
    glm::vec3(11250.3f, -3.3f, 70.0f),
	glm::vec3(12050.0f,  2.0f, -72.0f),
	glm::vec3(12850.0f,  0.0f, 100.0f),

	glm::vec3(350.7f,  10.2f,  -60.0f),
	glm::vec3(1300.3f, -10.3f, -160.0f),
	glm::vec3(2150.0f,  10.0f, -172.0f),
	glm::vec3(2900.0f,  10.0f, -80.0f),
    glm::vec3(3750.3f, -10.3f, -70.0f),
	glm::vec3(4500.0f,  10.0f, 72.0f),
	glm::vec3(5300.0f,  10.0f, -190.0f),
    glm::vec3(5650.3f, -10.3f, -70.0f),
	glm::vec3(5950.0f,  10.0f, 172.0f),
	glm::vec3(6450.0f,  10.0f, -100.0f),
    glm::vec3(6750.3f, -10.3f, 170.0f),
	glm::vec3(7050.0f,  10.0f, -72.0f),
	glm::vec3(7850.0f,  10.0f, 100.0f)
};  


//vertices of the skybox
float skyboxVertices[] = {
    // positions          
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f,  1.0f
};

//main begin
/////////////
int main(){
    //initialize GLFW
    glfwInit();
    //set some window hints
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);


    //create window object
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL){
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    //Address of OpenGL function pointers, OS specific
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


    //set size of rendering window
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    //callbacks
    //takes care of resizing
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);


    glEnable(GL_DEPTH_TEST);

    //shader files
    Shader lightingShader("resources/shaders/vertex.vert", "resources/shaders/fragment.frag", nullptr);
    Shader lampShader("resources/shaders/lamp.vert", "resources/shaders/lamp.frag", nullptr);
    Shader modelShader("resources/shaders/model.vert", "resources/shaders/model.frag", nullptr);
    Shader skyboxShader("resources/shaders/skybox.vert", "resources/shaders/skybox.frag", nullptr);

    //let's get to texturin'
    //------------------CONTAINER TEXTURES------------------
    unsigned int diffuseMap = loadTexture("resources/textures/container/container2.png");
    unsigned int specularMap = loadTexture("resources/textures/container/container2_specular.png");

    lightingShader.use();
    lightingShader.setInt("material.diffuse",0);
    //sets specular on the second texture
    lightingShader.setInt("material.specular",1);
    //load models
    Model ourModel("resources/objects/nanosuit/nanosuit.obj");
    Model playerModel("resources/objects/scorpio/vicviper12.obj");
    Model enemyModel("resources/objects/scorpio/vicviper12.obj");
    Model ringModel("resources/objects/ring/model.obj");
    Model samusModel("resources/objects/varia/DolBarriersuit.obj");
    
    //More models were supposed to be added, but the application gets a nasty segmentation fault every time they're imported
    //Try adding them for yourselves if you're curious, they're in the resources/objects folder


    //--------VAO/VBO-STUFF---------------------------------------------
    unsigned int VBO, lightVAO, lightVBO, wallVAO;
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &lightVBO);
    //light VAO
    glGenVertexArrays(1, &lightVAO);
    glGenVertexArrays(1, &wallVAO);

    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(wallVertices), &wallVertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    //enables location 0 (remember this for the shaders)
    glEnableVertexAttribArray(0);
    //normal attribute
     glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    //texture attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    //enables location 2 (remember this for the shaders)
    glEnableVertexAttribArray(2);  

    //bind light VAO
    glBindVertexArray(lightVAO);
    glBindBuffer(GL_ARRAY_BUFFER, lightVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);

    // we only need to bind to the VBO (to link it with glVertexAttribPointer), no need to fill it; the VBO's data already contains all we need (it's already bound, but we do it again for educational purposes)
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);


    //bind light VAO
    glBindVertexArray(wallVAO);
    glBindBuffer(GL_ARRAY_BUFFER, lightVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(wallVertices), &wallVertices, GL_STATIC_DRAW);

    //position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);


  // skybox VAO
    unsigned int skyboxVAO, skyboxVBO;
    glGenVertexArrays(1, &skyboxVAO);
    glGenBuffers(1, &skyboxVBO);
    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    
     ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 460");

    //------------SKYBOX-----------------
    std::vector<std::string> faces
    {
        "resources/textures/skybox/purplenebula_rt.tga",
        "resources/textures/skybox/purplenebula_lf.tga",
        "resources/textures/skybox/purplenebula_up.tga",
        "resources/textures/skybox/purplenebula_dn.tga",
        "resources/textures/skybox/purplenebula_ft.tga",
        "resources/textures/skybox/purplenebula_bk.tga"
    };
    unsigned int cubemapTexture = loadCubemap(faces);

    skyboxShader.use();
    skyboxShader.setInt("skybox", 0);


    //-------------RENDER-LOOP-------------------------
    while (!glfwWindowShouldClose(window)){
              //Background music
    SoundEngine->play2D("resources/sound/Corneria.mp3",GL_TRUE);
        //new imgui frame
        glfwPollEvents();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
         // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.2f, 0.2f, 0.6f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    

        //create matrices and set them to identity 
        glm::mat4 projection = glm::mat4(1.0f);
        projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        float cameraDistance = 30.0f; //Some value that is within your nearZ / farZ
        glm::vec3 cameraPosition(playerModel.Position.x, playerModel.Position.y,
                                             playerModel.Position.z + cameraDistance);
        //glm::mat4 view = camera.GetViewMatrix(playerModel.Position, cameraPosition);
        glm::mat4 view = camera.GetViewMatrixOriginal();

        // activate shader
        lightingShader.use();
        lightingShader.setMat4("projection", projection);

        //pass view to shader
        lightingShader.setMat4("view", view);

           // world transformation
        glm::mat4 model = glm::mat4(1.0f);
        lightingShader.setMat4("model", model);

        //update lighting 
        lightingShader.setVec3("lightPos", lightPos);
        //update the camera position
        lightingShader.setVec3("viewPos", camera.Position);

        //setting material
        lightingShader.setVec3("material.diffuse", 0.5f, 0.5f, 0.5f);
        lightingShader.setVec3("material.specular", 0.5f, 0.5f, 0.5f);
        lightingShader.setFloat("material.shininess", 64.0f);



        // directional light
        lightingShader.setVec3("dirLight.direction", -0.2f, -1.0f, -0.3f);
        lightingShader.setVec3("dirLight.ambient", 0.05f, 0.05f, 0.05f);
        lightingShader.setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
        lightingShader.setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);

        lightingShader.setVec3("pointLights[0].position", glm::vec3(ringModel.Position.x,ringModel.Position.y,ringModel.Position.z));
        lightingShader.setVec3("pointLights[0].ambient", 3.2f, 3.2f, 3.2f);
        lightingShader.setVec3("pointLights[0].diffuse", 8.0f, 8.0f, 8.0f);
        lightingShader.setVec3("pointLights[0].specular", 9.0f, 9.0f, 9.0f);
        lightingShader.setFloat("pointLights[0].constant", 1.0f);
        lightingShader.setFloat("pointLights[0].linear", 6.9);
        lightingShader.setFloat("pointLights[0].quadratic", 0.92);

        // point light 2
        lightingShader.setVec3("pointLights[1].position", 0.0f, 0.0f, 0.0f);
        lightingShader.setVec3("pointLights[1].ambient", 0.05f, 0.05f, 0.05f);
        lightingShader.setVec3("pointLights[1].diffuse", 0.8f, 0.0f, 0.0f);
        lightingShader.setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
        lightingShader.setFloat("pointLights[1].constant", 1.0f);
        lightingShader.setFloat("pointLights[1].linear", 0.09);
        lightingShader.setFloat("pointLights[1].quadratic", 0.032);
        // point light 3
        lightingShader.setVec3("pointLights[2].position", goldRingPositions[2]);
        lightingShader.setVec3("pointLights[2].ambient", 0.05f, 0.05f, 0.05f);
        lightingShader.setVec3("pointLights[2].diffuse", 0.8f, 0.8f, 0.8f);
        lightingShader.setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
        lightingShader.setFloat("pointLights[2].constant", 1.0f);
        lightingShader.setFloat("pointLights[2].linear", 0.09);
        lightingShader.setFloat("pointLights[2].quadratic", 0.032);
        // point light 4
        lightingShader.setVec3("pointLights[3].position", goldRingPositions[3]);
        lightingShader.setVec3("pointLights[3].ambient", 0.05f, 0.05f, 0.05f);
        lightingShader.setVec3("pointLights[3].diffuse", 0.8f, 0.8f, 0.8f);
        lightingShader.setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
        lightingShader.setFloat("pointLights[3].constant", 1.0f);
        lightingShader.setFloat("pointLights[3].linear", 0.09);
        lightingShader.setFloat("pointLights[3].quadratic", 0.032);

        //-----------PLAYER--------------------------------------
        //------------------------------
        
        model = glm::mat4(1.0f);
        lightingShader.init(projection, view);
        //change the model's position coordinates
       playerModel.updatePositionVectors(camera.Position+camera.Front*(glm::vec3(15.0f,15.0f,15.0f)) + glm::vec3(0.0f, -2.5f, 0.0f));

       //--BOOSTING-------------
       if (camera.boost){
           if (camera.smallBoost)
                camera.smallBoost = false;
        // point light 1

        //clock to change the color of the ship when it blueshifts
        double duration;
       //only start the clock if it's not already running
        if (start == -1)
            start = std::clock();

        duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        lightingShader.setVec3("pointLights[0].position", playerModel.Position.x, playerModel.Position.y + 4, playerModel.Position.z);
        lightingShader.setVec3("pointLights[0].ambient", 0.05f, 0.05f, 0.05f);
        lightingShader.setVec3("pointLights[0].diffuse", duration, duration, duration *4);
        lightingShader.setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
        lightingShader.setFloat("pointLights[0].constant", 1.0f);
        lightingShader.setFloat("pointLights[0].linear", 0.09f);
        lightingShader.setFloat("pointLights[0].quadratic", 0.032f);
       }
       else {
           start = -1;
       }
       //Acceleration with the spacebar
       if (camera.smallBoost){
           //std::cout << "Small boost!\n";
        // point light 1
        //clock to change the color of the ship's backside when it accelerates
        double duration;
        //only start the clock if it's not already running
        if (start == -1)
            start = std::clock();

        duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

        lightingShader.setVec3("pointLights[1].position", playerModel.Position.x - 5, playerModel.Position.y, playerModel.Position.z);
        lightingShader.setVec3("pointLights[1].ambient", 0.5f, 0.5f, 0.5f);
        lightingShader.setVec3("pointLights[1].diffuse", 0.4f, 0.0f, 0.0f);
        lightingShader.setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
        lightingShader.setFloat("pointLights[1].constant", 1.0f);
        lightingShader.setFloat("pointLights[1].linear", 0.09f);
        lightingShader.setFloat("pointLights[1].quadratic", 0.032f);
       }

        //puts the position vectors into glm(translate), the intented control scheme
        //model = glm::translate(model, playerModel.Position); // translate it down so it's at the center of the scene 
    
        //camera-stiched controls, used for testing
        model = glm::translate(model, camera.Position+camera.Front*(glm::vec3(15.0f,15.0f,15.0f)) + glm::vec3(0.0f, -2.5f, 0.0f));
        model = glm::scale(model,glm::vec3(0.05f, 0.05f, 0.05f));	// it's a bit too big for our scene, so scale it down
        //sets the initial degrees (facing away from the camera)
        model = glm::rotate(model, (float)(1.57), glm::vec3(0, 1, 0));

        //rotates on the yaw and pitch of the camera (using magic numbers for now, need to find something better eventually)
        //x rotation
        model = glm::rotate(model, (float)(camera.Yaw*0.018), glm::vec3(0,-1,0));
        //y rotation
        model = glm::rotate(model, (float)(camera.Pitch*0.016), glm::vec3(-1,0,0));
        lightingShader.setMat4("model", model);
        playerModel.Draw(lightingShader);
        /*std::cout << "ship x = "<< playerModel.Position.x << std::endl;
        std::cout << "ship y = "<< playerModel.Position.y << std::endl;
        std::cout << "ship z = "<< playerModel.Position.z << std::endl;*/
        if(playerModel.Position.x >= 3000 && enemyModel.Position.x < playerModel.Position.x)
            std::cout << "WINNER! " << std::endl;
        if (playerModel.Position.x > enemyModel.Position.x)
            std::cout << "In the lead! " << std::endl;

         //------NANOSUITS----------------------------------
        //------------------------------
               //create model matrix
        for (int i = 0; i <= 12; i++){
                        //set position values for further use
            ourModel.Position.x = nanosuitPositions[i].x;
            ourModel.Position.y = nanosuitPositions[i].y;
            ourModel.Position.z = nanosuitPositions[i].z;


                // activate shader
            lightingShader.use();
            glBindVertexArray(lightVAO);
            lightingShader.setMat4("projection", projection);

            //pass view to shader
            lightingShader.setMat4("view", view);

            // world transformation
            glm::mat4 model = glm::mat4(1.0f);
            lightingShader.setMat4("model", model);

            //update lighting 
            lightingShader.setVec3("lightPos", lightPos);
            //update the camera position
            lightingShader.setVec3("viewPos", camera.Position);

            //setting material
            lightingShader.setVec3("material.diffuse", 0.5f, 0.5f, 0.5f);
            lightingShader.setVec3("material.specular", 0.5f, 0.5f, 0.5f);
            lightingShader.setFloat("material.shininess", 64.0f);

            model = glm::mat4(1.0f);
            model = glm::translate(model, nanosuitPositions[i]);
            //rotate so it's the right angle
            model = glm::rotate(model, 1.6f, glm::vec3(0.0f, 0.0f, 1.0f));
            model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(0.0f, 5.0f, 0.0f));
            lightingShader.setMat4("model", model); 

            //draw the beauties
            ourModel.Draw(lightingShader);

        }

        //-------SAMUS-SUITS-------------------------------------

                   //create model matrix
        for (int i = 13; i <= 24; i++){
                        //set position values for further use
            samusModel.Position.x = nanosuitPositions[i].x;
            samusModel.Position.y = nanosuitPositions[i].y;
            samusModel.Position.z = nanosuitPositions[i].z;


                // activate shader
            lightingShader.use();
            glBindVertexArray(lightVAO);
            lightingShader.setMat4("projection", projection);

            //pass view to shader
            lightingShader.setMat4("view", view);

            // world transformation
            glm::mat4 model = glm::mat4(1.0f);
            lightingShader.setMat4("model", model);

            //update lighting 
            lightingShader.setVec3("lightPos", lightPos);
            //update the camera position
            lightingShader.setVec3("viewPos", camera.Position);

            //setting material
            lightingShader.setVec3("material.diffuse", 0.5f, 0.5f, 0.5f);
            lightingShader.setVec3("material.specular", 0.5f, 0.5f, 0.5f);
            lightingShader.setFloat("material.shininess", 64.0f);

            model = glm::mat4(1.0f);
            model = glm::translate(model, nanosuitPositions[i]);
            //rotate so it's the right angle
            model = glm::rotate(model, 1.6f, glm::vec3(1.0f, 0.0f, 0.0f));
            model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(0.0f, 5.0f, 0.0f));
            lightingShader.setMat4("model", model); 

            //draw the beauties
            samusModel.Draw(lightingShader);

        }


        //------------RINGS----------------------------------
        //--------------------------
        //create model matrix
        for (int i = 0; i <= 12; i++){
                        //set position values for further use
            ringModel.Position.x = goldRingPositions[i].x;
            ringModel.Position.y = goldRingPositions[i].y;
            ringModel.Position.z = goldRingPositions[i].z;


                // activate shader
            lightingShader.use();
            lightingShader.setMat4("projection", projection);

            //pass view to shader
            lightingShader.setMat4("view", view);

            // world transformation
            glm::mat4 model = glm::mat4(1.0f);
            lightingShader.setMat4("model", model);

            //update lighting 
            lightingShader.setVec3("lightPos", lightPos);
            //update the camera position
            lightingShader.setVec3("viewPos", camera.Position);

            //setting material
            lightingShader.setVec3("material.diffuse", 0.5f, 0.5f, 0.5f);
            lightingShader.setVec3("material.specular", 0.5f, 0.5f, 0.5f);
            lightingShader.setFloat("material.shininess", 64.0f);

            model = glm::mat4(1.0f);
            model = glm::translate(model, goldRingPositions[i]);
            //rotate so it's the right angle
            model = glm::rotate(model, 1.6f, glm::vec3(0.0f, 0.0f, 1.0f));
            model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(0.0f, 5.0f, 0.0f));
            lightingShader.setMat4("model", model); 

    //-----------------------COORDINATE-COLLISION--------------------
            //if they're in contact, go ahead and put the ship in hyperspeed
            if (playerModel.Position.x  < ringModel.Position.x &&  playerModel.Position.x > ringModel.Position.x - 7  &&
                playerModel.Position.y  < ringModel.Position.y + 11 &&  playerModel.Position.y > ringModel.Position.y - 11 &&
                playerModel.Position.z  < ringModel.Position.z  + 11 &&  playerModel.Position.z > ringModel.Position.z - 11)
                    camera.boost = true;

            if (camera.boost)
                camera.goldBoost(deltaTime);
                     //set the texture back

            //draw the beauties
            ringModel.Draw(lampShader);
        }
            
        //-------OTHER-SHIPS-----------------------------------
        //----------------------------
        //these 'ships' are merely dummy objects moving toward the goalpost,they do not possess any sort of health, speed or similar stats, nor do they have AI
        for (int i = 0; i <= 2; i++){
            // activate shader
            lightingShader.use();
            lightingShader.setMat4("projection", projection);

            //pass view to shader
            lightingShader.setMat4("view", view);

            model = glm::mat4(1.0f);
            lightingShader.init(projection, view);
            enemyModel.Position = glm::vec3(-130.0f + (glfwGetTime() * 100), -1.75f,-40.0f + (i*30));
            model = glm::translate(model,enemyModel.Position);
            model = glm::scale(model,glm::vec3(0.05f, 0.05f, 0.05f));	// it's a bit too big for our scene, so scale it down
            model = glm::rotate(model, 1.57f, glm::vec3(0.0f, 1.0f, 0.0f));
            lightingShader.setMat4("model", model);
            enemyModel.Draw(lightingShader);
        }

        //-------END-GOAL-------------------------
        //---------------------------
        //Create a wall of nanosuits to showcase where the endgoal is
        for (int i = 0; i<= 200; i++){
                //create model matrix
                //set position values for further use
                ourModel.Position.x = 3000;
                ourModel.Position.y = playerModel.Position.y;
                ourModel.Position.z = -300;


                    // activate shader
                lightingShader.use();
                glBindVertexArray(lightVAO);
                lightingShader.setMat4("projection", projection);

                //pass view to shader
                lightingShader.setMat4("view", view);

                // world transformation
                glm::mat4 model = glm::mat4(1.0f);
                lightingShader.setMat4("model", model);

                //update lighting 
                lightingShader.setVec3("lightPos", lightPos);
                //update the camera position
                lightingShader.setVec3("viewPos", camera.Position);

                //setting material
                lightingShader.setVec3("material.diffuse", 0.5f, 0.5f, 0.5f);
                lightingShader.setVec3("material.specular", 0.5f, 0.5f, 0.5f);
                lightingShader.setFloat("material.shininess", 64.0f);

                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(ourModel.Position.x, ourModel.Position.y, ourModel.Position.z + (i*10)));
                //rotate so it's the right angle
                lightingShader.setMat4("model", model); 

                //draw the beauties
                ourModel.Draw(lightingShader);
    
        }

        //----------OUTER-WALLS--------------------------------------
        //------------------------
        //the boundary of the racetrack, outside of this plane no one know what could happen 0_o
        lightingShader.use();
        lightingShader.setMat4("projection", projection);
        lightingShader.setMat4("view", view);

        glBindVertexArray(lightVAO);
        //create model matrix
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, -300.0f));
        lightingShader.setMat4("model", model);
        //draw the beauties
        glDrawArrays(GL_TRIANGLES, 0, 36);
      
         glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseMap);
        lightingShader.use();
        lightingShader.setMat4("projection", projection);
        lightingShader.setMat4("view", view);

        glBindVertexArray(lightVAO);
        //create model matrix
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 300.0f));
        lightingShader.setMat4("model", model);
        //draw the beauties
        glDrawArrays(GL_TRIANGLES, 0, 36);


        //------CHUNKS----------------------------------------------------------
        //-------------------------------
        //the procedural generation for the environment, segmentation fault and performance drop guaranteed. feel free to uncomment and experience the horror yourselves
        //this code would probably work if chunkArray[] actually was an array of chunk objects
        
       /* int chunkPosX = playerModel.Position.x + 100;
        int chunkPosY = playerModel.Position.y;
        int chunkPosZ = playerModel.Position.z;
        glm::vec3 chunkPos(chunkPosX,chunkPosY,chunkPosZ);

        //generating the rings' positional values    
        if ((chunkPosX % 221 == 0)){
                chunkArray[0].generate(chunkPos);
        }
        //spawning the  gold rings
            std::cout << "size of chunkArray: " << chunkArray.size() << std::endl;
            model = glm::mat4(1.0f);
            model = chunkArray[0].spawnRings();
            lightingShader.setMat4("model", model);
            ringModel.Draw(lightingShader);
        //spawning the asteroids
            model = glm::mat4(1.0f);
            model = chunkArray[0].spawnAsteroids();
            lightingShader.setMat4("model", model);
            ourModel.Draw(lightingShader);
        //spawning the silver rings
            model = glm::mat4(1.0f);
            model = chunkArray[0].spawnSilverRings();
            lightingShader.setMat4("model", model);
            playerModel.Draw(lightingShader);

        */



        //set the texture back
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseMap);

        //------RENDERING--SKYBOX-------
          // draw skybox as last
        glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
        skyboxShader.use();
       // view = glm::mat4(glm::mat3(camera.GetViewMatrix(playerModel.Position, cameraPosition))); // remove translation from the view matrix
        view = glm::mat4(glm::mat3(camera.GetViewMatrixOriginal()));
        skyboxShader.setMat4("view", view);
        skyboxShader.setMat4("projection", projection);
        // skybox cube
        glBindVertexArray(skyboxVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
        glDepthFunc(GL_LESS); // set depth function back to default



        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------


    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    
        if (show_demo_window)
           // ImGui::ShowDemoWindow(&show_demo_window);

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {
            static float f = 0.0f;
            static int counter = 0;

            ImGui::Begin("World coordinates");                          // Create a window called "Hello, world!" and append into it.
            //ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
            //ImGui::Checkbox("Another Window", &show_another_window);

            //ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
           // ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

            if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
                counter++;
            ImGui::Text("counter = %d", counter);
            ImGui::Text("X coordinate: %f", playerModel.Position.x);
            ImGui::Text("Y coordinate: %f", playerModel.Position.y);
            ImGui::Text("Z coordinate: %f", playerModel.Position.z);
            ImGui::Text("Speed: %f KM/h", camera.Zoom *(deltaTime * 3000));
            /*if(playerModel.Position.x >= 3000 && enemyModel.Position.x < playerModel.Position.x)
                ImGui::Text("WINNER!");
            if (playerModel.Position.x > enemyModel.Position.x)
                ImGui::Text("In the lead! ");*/

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            ImGui::End();
        }




       ImGui::Render();
             ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

   glfwPollEvents();
    glfwSwapBuffers(window);
    }

//cleans/deletes all the resources allocated  
ImGui_ImplOpenGL3_Shutdown();
ImGui_ImplGlfw_Shutdown();
ImGui::DestroyContext();
glDeleteVertexArrays(1, &lightVAO);
glDeleteBuffers(1, &VBO);
glDeleteVertexArrays(1, &skyboxVAO);
glDeleteBuffers(1, &skyboxVAO);

glfwTerminate();
return 0;
}
///////////
//main end

void framebuffer_size_callback(GLFWwindow* window, int width, int height){
    glViewport(0,0,width,height);
}

//input
void processInput(GLFWwindow* window){
    //moving forwards constantly
    camera.ProcessKeyboard(FORWARD, deltaTime);  
    //moving backwards
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    //moving to the left
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    //moving to the right
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
    //quit the window
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    
    //quit the window
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        camera.ProcessKeyboard(BOOST, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS){
        if (mousePointer){
          glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
          mousePointer = false;
        }
        if (!mousePointer){
          glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
          mousePointer = true;
        }
    }
    //continouous boost in an interval
    /*float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    int currentTime = glfwGetTime();
    if (currentTime % 3 == 0)
         camera.boost(deltaTime);*/
}
    
// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos){
    if (firstMouse){
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}


// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset){
    camera.ProcessMouseScroll(yoffset);
}

unsigned int loadTexture(char const * path){
    unsigned int textureID;
    glGenTextures(1, &textureID);
    
    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data){
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        //setting wrappers and filters for the texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //free up data
        stbi_image_free(data);
    }
    else{
        std::cout << "Texture failed to load at path " << path << std::endl;
        stbi_image_free(data);
    }
    return textureID;
}

unsigned int loadCubemap(std::vector<std::string> faces)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++)
    {
        unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID;
}