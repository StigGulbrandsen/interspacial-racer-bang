#include <chunk.h>
#include <iostream>

std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd());
    
//position of the chunk's objects, intended to be arrays but couldn't figure out a way to make vec3 arrays
glm::vec3 ringPos;
glm::vec3 asteroidPos;
glm::vec3 silverRingPos;

Chunk::Chunk(){
         //seed a random number for the number of gold rings
 
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1, 3);
    std::uniform_int_distribution<> dis2(1, 16);
    std::uniform_int_distribution<> dis3(1, 28);
    ringCount = dis(gen);
    //seed a random number for the number of asteroids
    asteroidCount = dis2(gen);
    //seed a random number for the number of silver rings
    silverCount = dis3(gen);
}
 Chunk::Chunk(glm::vec3 chunkPos){
 
    positionX = chunkPos.x; positionY = chunkPos.y; positionZ = chunkPos.z;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1, 3);
    std::uniform_int_distribution<> dis2(1, 16);
    std::uniform_int_distribution<> dis3(1, 28);
    ringCount = dis(gen);
    //seed a random number for the number of asteroids
    asteroidCount = dis2(gen);
    //seed a random number for the number of silver rings
    silverCount = dis3(gen);
   // for (int i = 0; i <= ringCount; i++)
      //  ringPos[i] = new glm::vec3;
 }

 Chunk::~Chunk(){
 }

//generating the random position of the objects
//this code would probably work in tangent with the loop in main if the position vectors were arrays, but my trials were fruitless in this regard
void Chunk::generate(glm::vec3 chunkPos){
   std::uniform_int_distribution<int> dis(chunkPos.x, chunkPos.x + 300);
    ringPos.x = dis(gen);
    ringPos.y = chunkPos.y;
    ringPos.z = 30;

    asteroidPos.x = dis(gen);
    asteroidPos.y = chunkPos.y;
    asteroidPos.z = 30;

    silverRingPos.x = dis(gen);
    silverRingPos.y = chunkPos.y;
    silverRingPos.z = 30;
 }

//spawns the various environmental objects, however  they're limited to 1 due to my own incompetence and will subsequently move to the chunk's new position
//once the player passes a certain boundary

//spawning gold rings
 glm::mat4 Chunk::spawnRings(){
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model,glm::vec3(ringPos.x, ringPos.y, ringPos.z));
     return model;
 }
//spawning asteroids
 glm::mat4 Chunk::spawnAsteroids(){
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model,glm::vec3(ringPos.x, ringPos.y, ringPos.z));
     return model;
 }
//spawning silver rings
 glm::mat4 Chunk::spawnSilverRings(){
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model,glm::vec3(ringPos.x, ringPos.y, ringPos.z));
     return model;
 }