#ifndef CHUNK_H
#define CHUNK_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <random>

//Chunk class, generates a random environment every 200 units once the player is close enough
class Chunk{
    private:
        int CHUNKW = 300;
        int CHUNKH = 300;
        float positionX, positionY, positionZ;
        //The number of objects
        int ringCount, asteroidCount, silverCount;
        //The position of the objects, their size will be determined by the aforementioned 
    public:
        Chunk();
        Chunk(glm::vec3 chunkPos);
        ~Chunk();
        //generate the position of the environmental objects
        void generate(glm::vec3 chunkPos);
        glm::mat4 spawnRings();
        glm::mat4 spawnAsteroids();
        glm::mat4 spawnSilverRings();
   };
#endif