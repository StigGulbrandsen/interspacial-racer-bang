
#Game
Interspacial Racer BANG is a racing game in space surprisingly enough, your goal is the reach the finish line (represented here by a line of nanosuits).

##Controls
The whole game can be controlled with just the mouse pointer. The buttons A and D give aditional rotational control in their respective directions. The ship can be accelerated with the spacebar, however
this speed alone is not enough to beat the other racers.

#Versioning
The whole development process is documented through commits, whenever a new feature was planned or finished, a commit was made.

This project was made and developed for the linux operating systems, if you wish to run these on another OS then you're on your own.

#Legacy
BANG is the codename for a game that I had originally planned for developing in 2D, in the style of GRADIUS but with the high speed racing of F-Zero. As the assignment focused on 3D development,
this idea was reworked for a 3D environment, in the style of Star FOX.

# Linux (Ubuntu)

#### Initial Setup
* Obtain Clang++ 5.0 (needed c++17 features, which the CMakeLists.txt currently assumes)
```bash
sudo apt-get install clang++-5.0
```
* Install CMake (`apt-get install cmake` will get you a fairly outdated version, so we'll obtain it manually instead)
```bash
#Remove previous installations of cmake
sudo apt-get remove --purge cmake
# make a temp folder for the CMake binaries
mkdir ~/temp 
cd ~/temp
# obtain the latest CMake version
wget https://cmake.org/files/v3.13/cmake-3.13.0-Linux-x86_64.tar.gz
# unzip the binaries
tar -xzvf cmake-3.13.0-Linux-x86_64.tar.gz
cd ~/temp/cmake-3.13.0-Linux-x86_64/
    
#place the binaries
cp -r ./bin     /usr/    
cp -r ./doc     /usr/share/
cp -r ./man     /usr/share/
cp -r ./share   /usr/

cmake --version  # verify the new installation:
                 # expected: cmake version 3.13.0
rm -r ~/temp     # cleanup temp directory, it's no longer needed
```
#### Building the Project
```bash
cd .../<Your_Labxx_Folder>
mkdir build/
cd build/
cmake ..
make
./<Your_Labxx_Folder>

# Configure and Generate CMake files.
# verify success by looking for "CMake configured Labxx successfully!"
# The "CMAKE_CXX_COMPILER" flag is used to set clang++ 5.0 as the compiler,
# (though any c++17 compatible compiler should in theory work here)
cmake .. -DCMAKE_CXX_COMPILER=clang++-5.0 
# Build the project
make
# Run the executable
./lab<xx>


