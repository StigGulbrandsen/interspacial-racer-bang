#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <ctime>
#include <vector>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    BOOST
};

// Default camera values
const float YAW         =  0.0f;
const float PITCH       =  0.0f;
const float SPEED       =  10.0f;
const float SENSITIVITY =  0.02f;
const float ZOOM        =  45.0f;


// An abstract camera class that processes input and calculates the corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // Euler Angles
    float Yaw;
    float Pitch;
    // Camera options
    float MovementSpeed;
    float MouseSensitivity;
    float Zoom;

    bool boost;
    bool smallBoost;
    // Constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(20), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        Position = position;
        WorldUp = up;
        Yaw = yaw;
        Pitch = pitch;
        updateCameraVectors();
    }
    // Constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        Position = glm::vec3(posX, posY, posZ);
        WorldUp = glm::vec3(upX, upY, upZ);
        Yaw = yaw;
        Pitch = pitch;
        updateCameraVectors();
    }

    // Returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetViewMatrix(glm::vec3 playerPos, glm::vec3 cameraPos)
    {

        return glm::lookAt(cameraPos, playerPos, Up);
    }

       // Returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetViewMatrixOriginal()
    {

        return glm::lookAt(Position, Position+Front, Up);
    }


    // Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboard(Camera_Movement direction, float deltaTime)
    {
        float velocity = MovementSpeed * deltaTime;
        if (direction == FORWARD){
                //reset the perspective when the ship is no longer boosting
                 if (Zoom > 45.0f && !boost && !smallBoost)
                    Zoom -= 1;
                if (Zoom < 45.0f)
                    Zoom = 45.0f;
            Position += Front * velocity;
            updateCameraVectors();
        }
        if (direction == BACKWARD){
            Position -= Front * velocity;
            updateCameraVectors();
        }
        if (direction == LEFT){
            Position -= Right * velocity;
            Yaw -= 30 * deltaTime;
            updateCameraVectors();
        }
        if (direction == RIGHT){
            Position += Right * velocity;
            Yaw += 30 * deltaTime;
            updateCameraVectors();
        }
        if (direction == BOOST){
            //warp the perspective once the ship accelerates
            if (Zoom < 60.0f)
                Zoom += 1.5;
            Position += Front * (velocity * 4);
            smallBoost = true;
        } 
        else {
            smallBoost = false;
        }
    }
    std::clock_t start;
    double duration;
    void goldBoost(float deltaTime){
        //timer for counting how long the boost lasts

        //only start the clock if it's not already running
        if (start <= -1 || start == NULL)
            start = std::clock();

        duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        //std::cout << "Boosting! time: " << duration << std::endl;

        
        //boosting through rings and other means 
        float velocity = SPEED * deltaTime;
        if (duration <= 4){
            //FOV zoom effect during high speeds
            if (Zoom < 130.0f)
                Zoom += 0.1f;
            //the FOV never stops, but at it's peak it increases much slower than before
            if (Zoom >= 130.0f)
                Zoom += 0.002f;

            Position += Front * (velocity);
            updateCameraVectors();
        }
        else {
            if (Zoom > 45.0f)
                Zoom -= 1;
             boost = false;
             start = -1;
        }
    }
    // Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true, GLboolean constrainYaw = true)
    {
        xoffset *= MouseSensitivity;
        yoffset *= MouseSensitivity;

        Yaw   += xoffset;
        Pitch += yoffset;

        // Make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch)
        {
            if (Pitch > 89.0f)
                Pitch = 89.0f;
            if (Pitch < -89.0f)
                Pitch = -89.0f;
        }
              // Make sure that when yaw is out of bounds, ship doesn't keep going
       /* if (constrainYaw)
        {
            if (Yaw > 60.0f)
                Yaw = 60.0f;
            if (Yaw < -60.0f)
                Yaw = -60.0f;
        }*/

        // Update Front, Right and Up Vectors using the updated Euler angles
        updateCameraVectors();
    }

    // Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void ProcessMouseScroll(float yoffset)
    {
        if (Zoom >= 1.0f && Zoom <= 45.0f)
            Zoom -= yoffset;
        if (Zoom <= 1.0f)
            Zoom = 1.0f;
        if (Zoom >= 45.0f)
            Zoom = 45.0f;
    }
private:
    // Calculates the front vector from the Camera's (updated) Euler Angles
    void updateCameraVectors()
    {
        // Calculate the new Front vector
        glm::vec3 front;
        front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        front.y = sin(glm::radians(Pitch));
        front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        Front = glm::normalize(front);
        // Also re-calculate the Right and Up vector
        Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
        Up    = glm::normalize(glm::cross(Right, Front));
    }
};
#endif