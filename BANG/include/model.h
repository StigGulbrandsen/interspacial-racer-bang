#ifndef MODEL_H
#define MODEL_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <mesh.h>
#include <stb_image.h>
#include <map>
#include <camera.h>

unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma = false);

class Model {
public:
        // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // Euler Angles
    float Yaw;
    float Pitch;
    // Camera options
    float MovementSpeed;
    float MouseSensitivity;
    float Zoom;
    float Speed;

    /*  Model Data */
    std::vector<Texture> textures_loaded;	// stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    std::vector<Mesh> meshes;
    std::string directory;
    bool gammaCorrection;
    bool playerCharacter;
    bool ring;

    /*  Functions   */
    // constructor, expects a filepath to a 3D model.
        Model(std::string const &path, bool gamma = false) : gammaCorrection(gamma){
        loadModel(path);
    }

    // draws the model, and thus all its meshes
    void Draw(Shader shader);
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch);
    void updatePositionVectors(glm::vec3 newPos);
    glm::mat4 GetViewMatrix();
    //determines if the model is a player or not
    glm::mat4 processMovement(Shader modelShader, Camera camera);
    void setRing();
    void ringContact(Model player, Camera camera, float deltatime);
    
private:

    // Default camera values
    const float YAW         =  90.0f;
    const float PITCH       =  0.0f;
    const float SPEED       =  2.5f;
    const float SENSITIVITY =  0.1f;
    const float ZOOM        =  45.0f;



    /*  Functions   */
    // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    void loadModel(std::string const &path);

    // processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void processNode(aiNode *node, const aiScene *scene);

    Mesh processMesh(aiMesh *mesh, const aiScene *scene);

    // checks all material textures of a given type and loads the textures if they're not loaded yet.
    // the required info is returned as a Texture struct.
    std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
};


unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma);
#endif