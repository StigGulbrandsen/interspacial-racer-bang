# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ribbreaker/Dropbox/openGLTutorial/src/gl3w.c" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/gl3w.c.o"
  "/home/ribbreaker/Dropbox/openGLTutorial/src/glad.c" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/glad.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "src"
  "lib/GFX"
  "lib/glew/include"
  "lib/glm"
  "lib/glfw/include"
  "lib/assimp-4.1.0/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ribbreaker/Dropbox/openGLTutorial/src/imgui.cpp" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/imgui.cpp.o"
  "/home/ribbreaker/Dropbox/openGLTutorial/src/imgui_draw.cpp" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/imgui_draw.cpp.o"
  "/home/ribbreaker/Dropbox/openGLTutorial/src/imgui_impl_glfw.cpp" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/imgui_impl_glfw.cpp.o"
  "/home/ribbreaker/Dropbox/openGLTutorial/src/imgui_impl_opengl3.cpp" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/imgui_impl_opengl3.cpp.o"
  "/home/ribbreaker/Dropbox/openGLTutorial/src/imgui_widgets.cpp" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/imgui_widgets.cpp.o"
  "/home/ribbreaker/Dropbox/openGLTutorial/src/main.cpp" "/home/ribbreaker/Dropbox/openGLTutorial/CMakeFiles/openGLTutorial.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "src"
  "lib/GFX"
  "lib/glew/include"
  "lib/glm"
  "lib/glfw/include"
  "lib/assimp-4.1.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
